#include <iostream>
#include <memory>
#include <FlameSteelCore/FSCUtils.h>
#include <FSGL/Controller/FSGLController.h>
#include <FlameSteelEngineGameToolkit/Data/Components/FSEGTFactory.h>
#include <FlameSteelEngineGameToolkitFSGL/Data/FSGTIOFSGLSystemFactory.h>
#include <FlameSteelEngineGameToolkit/IO/IOSystems/FSEGTIOSystemParams.h>

using namespace std;

int main(int argc, char *argv[]) {

	auto modelPath = make_shared<string>("com.demensdeum.deathmaskgame.shotgun.fsglmodel");

	auto ioSystemParams = make_shared<FSEGTIOSystemParams>();
	ioSystemParams->title = FSCUtils::localizedString(make_shared<string>("Skeletal Animation Tests"));
	ioSystemParams->width = 640;
	ioSystemParams->height = 480;
	ioSystemParams->windowed = true;

	auto controller = make_shared<FSGLController>();
      	controller->preInitialize();
	controller->initializeWindow(ioSystemParams);

	auto object = FSEGTFactory::makeOnSceneObject(
														            make_shared<string>("Model"),
														            make_shared<string>("Model"),
														            shared_ptr<string>(),
														            modelPath,
                                                                    					     shared_ptr<string>(),
														            0, 0, -4,
														            1, 1, 1,
														            0, 2, 0,
														            0);

	auto materialLibrary = make_shared<MaterialLibrary>();

	auto graphicsObject = FSGTIOFSGLSystemFactory::graphicsObjectFrom(object, materialLibrary);

	if (graphicsObject.get() == nullptr) {
		throw logic_error("graphics object is null, test failed");
	}

	controller->addObject(graphicsObject);
    
	graphicsObject->playAnimationWithName(make_shared<string>("Fire"));

 	auto startTime = time(nullptr);
    
	SDL_Event event;
	while(time(nullptr) - startTime < 4) {
		SDL_PollEvent(&event);
		controller->render();
	}

	return 0;

};
