cmake_minimum_required(VERSION 3.5)

project(SkeletalAnimationTests)

set(FSEGT_PROJECT_NAME SkeletalAnimationTests)

set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

set(CMAKE_VERBOSE_MAKEFILE ON)

if (APPLE)
link_directories(/usr/local/lib)
set(CMAKE_CXX_FLAGS "-Wall -Wextra -Werror=return-type -framework OpenGL")
elseif (EMSCRIPTEN)
set(CMAKE_CXX_FLAGS "--bind --use-preload-plugins -Wall -Wextra -Werror=return-type -s USE_SDL=2 -s USE_SDL_TTF=2 -s USE_SDL_IMAGE=2 -s USE_LIBPNG=1 -s DISABLE_EXCEPTION_CATCHING=0 -O3 --preload-file data/ -s ALLOW_MEMORY_GROWTH=1")
else()
set(CMAKE_CXX_FLAGS "-Wall -Wextra -Werror=return-type")
endif()

set(CMAKE_CXX_FLAGS_DEBUG "-g")
set(CMAKE_CXX_FLAGS_RELEASE "-O3")

set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/${FSEGT_PROJECT_NAME})
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/${FSEGT_PROJECT_NAME})
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/${FSEGT_PROJECT_NAME})
set(CMAKE_EXECUTABLE_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/${FSEGT_PROJECT_NAME})

if (APPLE)
include_directories(. FlameSteelFramework include /usr/local/include)
else()
include_directories(. FlameSteelFramework include)
endif()

file(GLOB_RECURSE SOURCE_FILES
    "FlameSteelFramework/*.cpp"
    "src/*.cpp"
)

set(EXECUTABLE_OUTPUT_PATH ${FSEGT_PROJECT_NAME})

add_executable(${FSEGT_PROJECT_NAME} ${SOURCE_FILES})

if (APPLE)
target_link_libraries(${FSEGT_PROJECT_NAME} SDL2 GLEW SDL2_ttf SDL2_mixer SDL2_image)
elseif(EMSCRIPTEN)
target_link_libraries(${FSEGT_PROJECT_NAME} GL GLEW)
else()
target_link_libraries(${FSEGT_PROJECT_NAME} SDL2 GL GLEW SDL2_ttf SDL2_mixer SDL2_image)
endif()
